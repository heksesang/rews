use directories::ProjectDirs;
use once_cell::sync::OnceCell;

pub mod article;
pub mod configure;
pub mod connection;
pub mod download;
pub mod nzb;
pub mod queue;

pub static PROJECT_DIR: OnceCell<ProjectDirs> = OnceCell::new();
